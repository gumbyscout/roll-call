<?php
//this adds the group to the database

//rollcall stuff
require_once('rollcall_miscfunctions.php');

//usercake stuff
require_once('models/config.php');
if (!securePage($_SERVER['PHP_SELF'])){die();}

//make sure name is filled in
if(empty($_POST['name'])){
	$_SESSION['addgrouperr'] = "Name must not be blank!";
	header("location:rollcall_add_group.php");
	exit();
}

//get values from previous form
$name = $_POST['name'];
$groupStatusID = $_POST['groupStatusID'];
$facilityID = $_POST['facility'];

//insert into database
$stmt = $mysqli->prepare("INSERT INTO groups(name, groupStatusID, facilityID) VALUES(?, ?, ?)");
$stmt->bind_param('sss', $namepara, $groupStatusIDpara, $facilityIDpara);

//assign values for parameters
$namepara = $name;
$groupStatusIDpara = $groupStatusID;
$facilityIDpara = $facilityID;

//execute
$stmt->execute();

//return to groups page
header("location:rollcall_display_groups.php");
?>
