<?php
//add a single group with default status of 'none'

//rollcall stuff
require_once('rollcall_miscfunctions.php');

//usercake stuff
require_once('models/config.php');
if (!securePage($_SERVER['PHP_SELF'])){die();}
require_once('models/header.php');
echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>RollCall</h1>
<h2>Add Group</h2>
<div id='left-nav'>";
include("left-nav.php");

//do queries for dropdown boxes
$facs = getFacilities($mysqli);
?>
</div>
<div id="main">
<?php
//for error of no name
if(isset($_SESSION['addgrouperr'])){
?>
<p class="error">
	<?php echo $_SESSION['addgrouperr']; unset($_SESSION['addgrouperr']); ?>
</p>
<?php
}
?>
<form method="POST" action="rollcall_add_group_process.php">
	<table>
		<tr>
			<td>Name:</td>
			<td><input type="text" name="name"></td>
		
		
		<tr>
		<?php 
		//same as above with groups, for debug adding
		if(!isset($loggedInUser->facilityID)){
		?>
		<tr>
			<td>Facility:</td>
			<td><?php populateDropdown($facs, 'facility'); ?></td>
		</tr>
		<?php
		}
		else{
		?>
		<input type="hidden" value="<?php echo $loggedInUser->facilityID; ?>"
			name="facility">
		<?php
		}
		?>
	</table>
	<input type="hidden" name="groupStatusID" value="1"> <input
		type="submit" value="Add Group">
</form>
</div>
</body>
