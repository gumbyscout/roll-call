<?php
//rollcall stuff
require_once('rollcall_miscfunctions.php');

//usercake stuff
require_once('models/config.php');
if (!securePage($_SERVER['PHP_SELF'])){die();}
require_once('models/header.php');
echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>RollCall</h1>
<h2>Students</h2>
<div id='left-nav'>";
include("left-nav.php");

$students = getStudents($loggedInUser->facilityID, $mysqli);

?>
</div>
<div id="main">
<table>
	<tr>
		<th>Name</th>
		<th>Group</th>
		<th>Destination</th>
		<th>Status</th>
	</tr>
	<?php
	//print out students
	foreach($students as $stu){
	?>
	<tr>
		<td><?php echo $stu['name']; ?></td>
		<td><?php $group = getGroup($stu['groupID'], $mysqli); echo $group['name']; ?></td>
		<td><?php $destination = getFacility($stu['destinationID'], $mysqli); echo $destination['name']; ?></td>
		<td>#TODO</td>
	</tr>
	<?php
	}
	?>
</table>
<a href="rollcall_add_student.php">Add Student</a>
</div>
</body>

