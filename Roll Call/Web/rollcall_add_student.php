<?php
//this is for adding a single student
//rollcall stuff
require_once('rollcall_miscfunctions.php');

//usercake stuff
require_once('models/config.php');
if (!securePage($_SERVER['PHP_SELF'])){die();}
require_once('models/header.php');
echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>RollCall</h1>
<h2>Add Student</h2>
<div id='left-nav'>";
include("left-nav.php");

//do queries for dropdown boxes
$facs = getFacilities($mysqli);

//allow debug adding, but also adding with session
if(isset($loggedInUser->facilityID)){
	$groups = getGroups($loggedInUser->facilityID, $mysqli);

	//get destinations, i.e. everything but school
	$dests = array();
	foreach($facs as $fac){
		if($fac['id'] != $loggedInUser->facilityID){
			$dests[] = $fac;
		}
	}
}
else{
	$groups = getAllGroups($mysqli);
	$dests = $facs;
}
?>
</div>
<div id="main">
<form method="POST" action="rollcall_add_student_process.php">
	<?php 
	//use this to display no name entered
	if(isset($_SESSION['addstuerr'])){
	?>
	<tr>
		<td class="error"><?php echo $_SESSION['addstuerr']; unset($_SESSION['addstuerr']); ?>
		</td>
	</tr>
	<?php
		}
	?>
	<table>
		<tr>
			<td>Name:</td>
			<td><input type="text" name="name"></td>
		</tr>
		<tr>
			<td>Group:</td>
			<td><?php populateDropdown($groups, 'group'); ?></td>
		</tr>
		<?php 
		//same as above with groups, for debug adding
		if(!isset($loggedInUser->facilityID)){
		?>
		<tr>
			<td>School:</td>
			<td><?php populateDropdown($facs, 'school'); ?></td>
		</tr>
		<?php
		}
		else{
		?>
		<input type="hidden" value="<?php echo $loggedInUser->facilityID; ?>"
			name="school">
		<?php
		}
		?>
		<tr>
			<td>Destination:</td>
			<td><?php populateDropdown($dests, 'destination'); ?></td>
		</tr>
			
	</table>
	<input type="submit" value="Add Student">
</form>
</div>
</body>
