<?php
//this actually adds the student to database

//rollcall stuff
require_once('rollcall_miscfunctions.php');

//usercake stuff
require_once('models/config.php');
if (!securePage($_SERVER['PHP_SELF'])){die();}


//make sure name is filled in and that source and destination aren't the same!
if(empty($_POST['name'])){
	$_SESSION['addstuerr'] = "Name must not be blank!";
	header("location:rollcall_add_student.php");
	exit();
}
else if($_POST['school'] == $_POST['destination']){
	$_SESSION['addstuerr'] = "Destination must be different from source!";
	header("location:rollcall_add_student.php");
	exit();
}

//get stuff from previous form
$name = $_POST['name'];
$group = $_POST['group'];
$school= $_POST['school'];
$destination = $_POST['destination'];

//insert into database
$stmt = $mysqli->prepare("INSERT INTO students(name, groupID, schoolID, destinationID) VALUES(?, ?, ?, ?)");
$stmt->bind_param('ssss', $namepara, $groupID, $schoolID, $destinationID);

//assign values for parameters
$namepara = $name;
$groupID = $group;
$schoolID = $school;
$destinationID = $destination;

//execute
$stmt->execute();

//redirect to student page
header("location:rollcall_display_students.php");

?>
