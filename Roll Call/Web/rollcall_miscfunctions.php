<?php
function getNumStuInGroup($groupID, $db){
	//retuns the number of students in specified group
	$querystr = "SELECT id, schoolID, destinationID, familyID, groupID, name
               FROM students 
               WHERE groupID = $groupID";
	$result = $db->query($querystr);
	$students = array();
	while ( $row = $result->fetch_assoc() ){
		$students[] = $row;
	}
	$result->close();
	return count($students);
}

function getNumStuInFamily($familyID, $db){
	//retuns the number of students in specified family
	$querystr = "SELECT id, schoolID, destinationID, familyID, groupID, name
               FROM students 
               WHERE familyID = $familyID";
	$result = $db->query($querystr);
	$students = array();
	while ( $row = $result->fetch_assoc() ){
		$students[] = $row;
	}
	$result->close();
	return count($students);
}

function getGroupStatus($statusID, $db){
	//returns the status of the group as an array
	$querystr = "SELECT id, name 
               FROM groupstatus 
               WHERE id = $statusID";
	$result = $db->query($querystr);
	$row = $result->fetch_assoc();
	$result->close();
	return 	$row;
}

function getGroup($grouID, $db){
	//returns a single row as an assoc array
	$querystr = "SELECT id, facilityID, groupStatusID, name 
               FROM groups 
               WHERE id = $grouID";
	$result = $db->query($querystr);
	$row = $result->fetch_assoc();
	$result->close();
	return 	$row;
}

function getAllGroups($db){
	//returns an array of assoc arrays for all groups
	$querystr = "SELECT id, facilityID, groupStatusID, name 
               FROM groups";
	$result = $db->query($querystr);
	$groups = array();
	while ( $row = $result->fetch_assoc() ){
		$groups[] = $row;
	}
	$result->close();
	return $groups;
}

function getGroups($facilityID, $db){
	//returns an array of assoc arrays for all groups owned by facility
	$querystr = "SELECT id, facilityID, groupStatusID, name 
               FROM groups 
               WHERE facilityID = $facilityID";
	$result = $db->query($querystr);
	$groups = array();
	while ( $row = $result->fetch_assoc() ){
		$groups[] = $row;
	}
	$result->close();
	return $groups;
}

function getFamily($familyID, $db){
	//returns a single row as an assoc array
	$querystr = "SELECT id, name 
               FROM families 
               WHERE id = $familyID";
	$result = $db->query($querystr);
	$row = $result->fetch_assoc();
	$result->close();
	return 	$row;
}

function getFamilies($facilityID, $db){
	//returns an array of assoc arrays for all families owned by facility
	$querystr = "SELECT id, name
               FROM families 
               WHERE facilityID = $facilityID";
	$result = $db->query($querystr);
	$families = array();
	while ( $row = $result->fetch_assoc() ){
		$families[] = $row;
	}
	$result->close();
	return $families;
}

function getMonitors($facilityID, $db){
	//returns an array of assoc arrays for all groups owned by facility
	$querystr = "SELECT * FROM Monitors WHERE facilityID = $facilityID";
	$result = $db->query($querystr);
	$monitors = array();
	while ( $row = $result->fetch_assoc() ){
		$monitors[] = $row;
	}
	$result->close();
	return $monitors;
}

function getGuardians($facilityID, $db){
	//returns an array of assoc arrays for all groups owned by facility
	$querystr = "SELECT * FROM Guardians WHERE facilityID = $facilityID";
	$result = $db->query($querystr);
	$guardians = array();
	while ( $row = $result->fetch_assoc() ){
		$guardians[] = $row;
	}
	$result->close();
	return $guardians;
}

function getStudent($studentID, $db){
	//TODO
}

function getAllStudents($db){
	//returns an array of assoc arrays for all students
	$querystr = "SELECT id, schoolID, destinationID, familyID, groupID, name 
               FROM students";
	$result = $db->query($querystr);
	$students = array();
	while ( $row = $result->fetch_assoc() ){
		$students[] = $row;
	}
	$result->close();
	return $students;
}

function getStudents($facilityID, $db){
	//returns an array of assoc arrays for all students owned by facility
	$querystr = "SELECT id, schoolID, destinationID, familyID, groupID, name
               FROM students 
               WHERE schoolID = $facilityID";
	$result = $db->query($querystr);
	$students = array();
	while ( $row = $result->fetch_assoc() ){
		$students[] = $row;
	}
	$result->close();
	return $students;
}

function getFacility($facID, $db){
	//returns a single row as an assoc array
	$querystr = "SELECT id, name, facilityType
               FROM facilities 
               WHERE id = $facID";
	$result = $db->query($querystr);
	$row = $result->fetch_assoc();
	$result->close();
	return 	$row;
}

function getFacilities($db){
	//returns an array of assoc arrays for all facilities
	$querystr = "SELECT id, name, facilityType 
               FROM facilities";
	$result = $db->query($querystr);
	$facs = array();
	while ( $row = $result->fetch_assoc() ){
		$facs[] = $row;
	}
	$result->close();
	return $facs;
}

function populateDropdown($assoarr, $name){
	//creates a dropdown list with an associative array
	echo "<select name='$name'>";
	foreach($assoarr as $row){
		echo "<option value='" . $row['id'] . "'>" . $row['name']. "</option>\n";
	} 
	echo '</select>';
}
?>
