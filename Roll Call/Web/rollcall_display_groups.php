<?php
//rollcall stuff
require_once('rollcall_miscfunctions.php');

//usercake stuff
require_once('models/config.php');
if (!securePage($_SERVER['PHP_SELF'])){die();}
require_once('models/header.php');
echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>RollCall</h1>
<h2>Groups</h2>
<div id='left-nav'>";
include("left-nav.php");

//get groups
$groups = getGroups($loggedInUser->facilityID, $mysqli);

?>
</div>
<div id="main">
<table>
<tr>
	<th>Name</th>
	<th>Status</th>
	<th>Students</th>
</tr>
<?php 
//populate table
foreach($groups as $row){
?>
<tr>
	<td><?php echo $row['name']; ?></td>
	<td><?php $groupstatus = getGroupStatus($row['groupStatusID'], $mysqli); echo $groupstatus['name']; ?></td>
	<td><?php echo getNumStuInGroup($row['id'], $mysqli); ?></td>
</tr>
<?php 
}
?>
</table>
<a href="rollcall_add_group.php">Add Group</a>
</div>
</body>
