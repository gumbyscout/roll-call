-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 17, 2013 at 01:53 AM
-- Server version: 5.6.13
-- PHP Version: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rollcalldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE IF NOT EXISTS `facilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `facilityType` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `facilityType` (`facilityType`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `name`, `facilityType`) VALUES
(1, 'Austin Peay', 1),
(2, 'Minglewood Elementary', 1),
(3, 'Gingerbread House', 2),
(4, 'Little Angels', 2),
(5, 'Student''s Home', 3);

-- --------------------------------------------------------

--
-- Table structure for table `facilitytype`
--

CREATE TABLE IF NOT EXISTS `facilitytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `facilitytype`
--

INSERT INTO `facilitytype` (`id`, `name`) VALUES
(1, 'School'),
(2, 'Daycare'),
(3, 'Home');

-- --------------------------------------------------------

--
-- Table structure for table `families`
--

CREATE TABLE IF NOT EXISTS `families` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `families`
--

INSERT INTO `families` (`id`, `name`) VALUES
(1, 'Malliet'),
(2, 'Fawkes');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facilityID` int(11) NOT NULL,
  `groupStatusID` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `facilityID` (`facilityID`),
  KEY `groupStatusID` (`groupStatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `facilityID`, `groupStatusID`, `name`) VALUES
(1, 1, 1, 'Bus 1'),
(2, 2, 1, 'Bus 2'),
(3, 1, 1, 'Zoo 9');

-- --------------------------------------------------------

--
-- Table structure for table `groupstatus`
--

CREATE TABLE IF NOT EXISTS `groupstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groupstatus`
--

INSERT INTO `groupstatus` (`id`, `name`) VALUES
(1, 'None'),
(2, 'Red'),
(3, 'Yellow');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schoolID` int(11) NOT NULL,
  `destinationID` int(11) NOT NULL,
  `familyID` int(11) DEFAULT NULL,
  `groupID` int(11) DEFAULT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `schoolID` (`schoolID`),
  KEY `destinationID` (`destinationID`),
  KEY `familyID` (`familyID`),
  KEY `groupID` (`groupID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `schoolID`, `destinationID`, `familyID`, `groupID`, `name`) VALUES
(1, 1, 3, 2, 1, 'Saul Fawkes'),
(2, 2, 4, 1, 2, 'Lilly Malliet'),
(3, 2, 4, 1, 2, 'Mark Malliet'),
(4, 1, 2, NULL, 1, 'Marie Von Camp'),
(5, 1, 2, NULL, 1, 'Mark Big Berug');

-- --------------------------------------------------------

--
-- Table structure for table `studentstatus`
--

CREATE TABLE IF NOT EXISTS `studentstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `studentstatus`
--

INSERT INTO `studentstatus` (`id`, `name`) VALUES
(1, 'None'),
(2, 'Orange'),
(3, 'Green');

-- --------------------------------------------------------

--
-- Table structure for table `studentstatushistory`
--

CREATE TABLE IF NOT EXISTS `studentstatushistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statusID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `statusDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `statusID` (`statusID`),
  KEY `studentID` (`studentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `uc_configuration`
--

CREATE TABLE IF NOT EXISTS `uc_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `value` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `uc_configuration`
--

INSERT INTO `uc_configuration` (`id`, `name`, `value`) VALUES
(1, 'website_name', 'RollCall'),
(2, 'website_url', 'localhost/'),
(3, 'email', 'noreply@ILoveUserCake.com'),
(4, 'activation', 'false'),
(5, 'resend_activation_threshold', '0'),
(6, 'language', 'models/languages/en.php'),
(7, 'template', 'models/site-templates/default.css');

-- --------------------------------------------------------

--
-- Table structure for table `uc_pages`
--

CREATE TABLE IF NOT EXISTS `uc_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(150) NOT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `uc_pages`
--

INSERT INTO `uc_pages` (`id`, `page`, `private`) VALUES
(1, 'account.php', 1),
(2, 'activate-account.php', 0),
(3, 'admin_configuration.php', 1),
(4, 'admin_page.php', 1),
(5, 'admin_pages.php', 1),
(6, 'admin_permission.php', 1),
(7, 'admin_permissions.php', 1),
(8, 'admin_user.php', 1),
(9, 'admin_users.php', 1),
(10, 'forgot-password.php', 0),
(11, 'index.php', 0),
(12, 'left-nav.php', 0),
(13, 'login.php', 0),
(14, 'logout.php', 1),
(15, 'register.php', 0),
(16, 'resend-activation.php', 0),
(17, 'user_settings.php', 1),
(18, 'example.php', 0),
(19, 'rollcall_add_group.php', 1),
(20, 'rollcall_add_group_process.php', 1),
(21, 'rollcall_add_student.php', 1),
(22, 'rollcall_add_student_process.php', 1),
(24, 'rollcall_dbconnect.php', 1),
(25, 'rollcall_display_groups.php', 1),
(28, 'rollcall_display_students.php', 1),
(31, 'rollcall_miscfunctions.php', 0);

-- --------------------------------------------------------

--
-- Table structure for table `uc_permissions`
--

CREATE TABLE IF NOT EXISTS `uc_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `uc_permissions`
--

INSERT INTO `uc_permissions` (`id`, `name`) VALUES
(1, 'New Member'),
(2, 'Global Administrator'),
(3, 'Guardian'),
(4, 'Monitor'),
(5, 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `uc_permission_page_matches`
--

CREATE TABLE IF NOT EXISTS `uc_permission_page_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `uc_permission_page_matches`
--

INSERT INTO `uc_permission_page_matches` (`id`, `permission_id`, `page_id`) VALUES
(1, 1, 1),
(2, 1, 14),
(3, 1, 17),
(4, 2, 1),
(5, 2, 3),
(6, 2, 4),
(7, 2, 5),
(8, 2, 6),
(9, 2, 7),
(10, 2, 8),
(11, 2, 9),
(12, 2, 14),
(13, 2, 17),
(14, 2, 19),
(15, 2, 20),
(16, 2, 21),
(17, 2, 22),
(18, 2, 24),
(19, 3, 24),
(20, 4, 24),
(21, 2, 25),
(22, 4, 25),
(23, 5, 25),
(24, 2, 28),
(25, 3, 28),
(26, 4, 28),
(27, 5, 28);

-- --------------------------------------------------------

--
-- Table structure for table `uc_users`
--

CREATE TABLE IF NOT EXISTS `uc_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(150) NOT NULL,
  `activation_token` varchar(225) NOT NULL,
  `last_activation_request` int(11) NOT NULL,
  `lost_password_request` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `title` varchar(150) NOT NULL,
  `sign_up_stamp` int(11) NOT NULL,
  `last_sign_in_stamp` int(11) NOT NULL,
  `facilityID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `uc_users`
--

INSERT INTO `uc_users` (`id`, `user_name`, `display_name`, `password`, `email`, `activation_token`, `last_activation_request`, `lost_password_request`, `active`, `title`, `sign_up_stamp`, `last_sign_in_stamp`, `facilityID`) VALUES
(1, 'gumbyscout', 'gumbyscout', 'd6135297b0efe3bf0ea3e1a30cc59fa0c148687fd6a64fbcafdb4a17d47ef9325', 'gumbyscout@gmail.com', '91410ff2a198e2b752f42ddd7051d1c0', 1379196428, 0, 1, 'Admin', 1379196428, 1381991164, 1);

-- --------------------------------------------------------

--
-- Table structure for table `uc_user_permission_matches`
--

CREATE TABLE IF NOT EXISTS `uc_user_permission_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `uc_user_permission_matches`
--

INSERT INTO `uc_user_permission_matches` (`id`, `user_id`, `permission_id`) VALUES
(1, 1, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_ibfk_1` FOREIGN KEY (`facilityType`) REFERENCES `facilitytype` (`id`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`facilityID`) REFERENCES `facilities` (`id`),
  ADD CONSTRAINT `groups_ibfk_2` FOREIGN KEY (`groupStatusID`) REFERENCES `groupstatus` (`id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`schoolID`) REFERENCES `facilities` (`id`),
  ADD CONSTRAINT `students_ibfk_2` FOREIGN KEY (`destinationID`) REFERENCES `facilities` (`id`),
  ADD CONSTRAINT `students_ibfk_3` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`),
  ADD CONSTRAINT `students_ibfk_4` FOREIGN KEY (`groupID`) REFERENCES `groups` (`id`);

--
-- Constraints for table `studentstatushistory`
--
ALTER TABLE `studentstatushistory`
  ADD CONSTRAINT `studentstatushistory_ibfk_1` FOREIGN KEY (`statusID`) REFERENCES `studentstatus` (`id`),
  ADD CONSTRAINT `studentstatushistory_ibfk_2` FOREIGN KEY (`studentID`) REFERENCES `students` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
